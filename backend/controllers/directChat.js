const DirectChatModel = require("../models/chat/directChat.model");
const httpStatusCode = require("../common/httpStatusCode");
const { STATES } = require("mongoose");

/**GET THE LIST OF DIRECT CHATS
 *
 * @param {any} req
 * @param {any} res
 */
const getUserDirectChats = async (req, res) => {
	console.log("req body", req.body);
	const { user_id, token } = req.body;

	const directChats = await DirectChatModel.find({
		$and: [
			{ $or: [{ owner_id: user_id }, { reciever_id: user_id }] },
			{ conversation: { $size: { $gt: 0 } } },
		],
	});

	if (directChats) {
		res.status(httpStatusCode.OK).json(directChats);
		res.end();
	} else {
		res.status(httpStatusCode.NOT_FOUND).json({
			error:
				"direct chats not found , maybe you dont have a conversation with someone yet.",
		});
		throw new Error("direct chats for this user , not found.".red.bgYellow);
	}
};

const startDirectChat = (req, res) => {};

const sendMessage = async (req, res) => {};

module.exports = {
	getUserDirectChats,
};
