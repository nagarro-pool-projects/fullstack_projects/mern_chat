const bcrypt = require("bcryptjs");
const asyncHandler = require("express-async-handler");
const User = require("../../models/user.model");
const { generateToken } = require("../../common/jwt");

//Register new User
const registerUser = asyncHandler(async (req, res) => {
  console.log("req.file");
  console.log(req.file);
  const {
    first_name,
    last_name,
    email,
    password,
    username,
    customer_id,
    team_id,
  } = req.body;

  const { path } = req.file === undefined ? "" : req.file;
  //Required fields
  if (
    !first_name ||
    !last_name ||
    !username ||
    !email ||
    !password ||
    !customer_id ||
    !team_id
  ) {
    res.status(400);
    throw new Error("Please add all fields".red.bgYellow);
  }

  //Check if email exist
  const userExist = await User.findOne({ email: email });
  if (userExist) {
    console.error("User already exist.".red.bgYellow);
    res.status(400).json({ error: "User already exist." });
    return;
  }

  //Hash password
  const salt = await bcrypt.genSalt(11);
  const hashedPassword = await bcrypt.hash(password, salt);

  //Save user info to db
  User.create({
    first_name,
    last_name,
    username,
    email,
    password: hashedPassword,
    profile_picture_path: path,
    customer_id: customer_id,
    team_id: team_id,
  })
    .then(async (response) => {
      if (response) {
        const token = await generateToken(response._id);
        res.status(201).json({
          message: "created succesfully",
          user: { ...response, token },
        });
      }
    })
    .catch((error) => {
      res.status(400).json({ error: error.message });
      throw new Error("Invalid user data...".red.bgYellow);
    });
});

module.exports = {
  registerUser,
};
