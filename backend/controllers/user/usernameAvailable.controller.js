const asyncHandler = require("express-async-handler");
const User = require('../../models/user.model');

const usernameAvailable = asyncHandler(async(req, res)=>{
    const {username} = req.body;
    console.log(username);
      //Check if username exist
      const usernameExist = await User.findOne({ username });
      if (usernameExist) {
        const { username, _id } = usernameExist;
        // console.log(usernameExist);
        res.status(200).json({
          ok: false,
          message: "username is already taken",
          user: { username, _id },
        });
      } else {
        res.status(200).json({ ok: true, message: "username is available" });
      }
  });

module.exports = {
    usernameAvailable
}