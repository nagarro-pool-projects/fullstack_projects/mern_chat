const bcrypt = require("bcryptjs")
const jwt = require("jsonwebtoken")
const User = require("../../models/user.model")
const { generateToken } = require("../../common/jwt")
const asyncHandler = require("express-async-handler")

/**
 * AUTH METHOD FOR USER
 * @param req
 * @param res
 */
const logIn = asyncHandler(async (req, res) => {
	const { email, password } = req.body

	if (!email || !password) {
		res.status(400).json({ error: "Please add all fields" })
		throw new Error("Please add all fields".red.bgYellow)
	}

	const user = await User.findOne({ email: email })

	if (user && (await bcrypt.compare(password, user.password))) {
		res.status(200).json({
			id: user._id,
			username: user.username,
			first_name: user.first_name,
			last_name: user.last_name,
			email: user.email,
			profile_picture_path: user.profile_picture_path,
			online: user.online,
			token: await generateToken(user._id),
		})
	} else {
		res.status(401).json({ error: "Invalid email or password" })
		throw new Error("Invalid email or password".red.bgYellow)
	}
})

/** adding extra comments here...
 * GET THE CURRENT LOGED USER
 * @param req
 * @param res
 */
const getLogedUser = asyncHandler(async (req, res) => {
	if (req.headers["x-auth-token"]) {
		const token = req.headers["x-auth-token"]

		//Verify token
		jwt.verify(token, process.env.JWT_SECRET, (err, decoded) => {
			if (err) {
				res.status(401).json({ error: "Not Authorized, user token has expired..." })
				throw new Error("Not authorized, user token has expired...".red.bold)
			} else {
				//Get user id from decoded token
				User.findById(decoded.id)
					.select("-password")
					.then((response) => {
						res.json({
							id: response._id,
							username: response.username,
							first_name: response.first_name,
							last_name: response.last_name,
							email: response.email,
							profile_picture_path: response.profile_picture_path,
							online: response.online,
						})
					})
					.catch((err) => {
						console.log("user db error", err)

						res.status(401).json({ error: "Unable to fetch user data..." })
						throw new Error("Unable to fetch user data...")
					})
			}
		})
	} else {
		res.status(401).json({ error: "Not Authorized, no token found..." })
		throw new Error("Not authorized, no token found...".red.bold)
	}
})

module.exports = {
	logIn,
	getLogedUser,
}
