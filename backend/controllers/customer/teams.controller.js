const TeamsModel = require("../../models/customer/teams.model");
const httpStatusCode = require("../../common/httpStatusCode");
const ObjectId = require("mongoose").Types.ObjectId;

const addNewTeam = async (req, res, next) => {
  const { name, customer_id } = req.body;
  if (!name || !customer_id) {
    res
      .status(httpStatusCode.BAD_REQUEST)
      .json({ error: "Please add all fields" });
    throw new Error("Please add all fields".red.bgYellow);
  }

  if (customer_id && !ObjectId.isValid(customer_id)) {
    res
      .status(httpStatusCode.BAD_REQUEST)
      .json({ error: "Please add a valid customer_id" });
    throw new Error("Please add a valid customer_id".red.bgYellow);
  }

  //Check if team already exist
  const teamFinded = await TeamsModel.findOne({ name });
  //   console.log("teamFinded");
  //   console.log(teamFinded);
  if (
    teamFinded &&
    ObjectId(teamFinded.customer_id).toString() ==
      ObjectId(customer_id).toString()
  ) {
    console.error(
      "team already exist ,actually matching with the incoming customer_id.".red
        .bgYellow
    );
    res.status(httpStatusCode.BAD_REQUEST).json({
      ok: false,
      error:
        "team already exist ,actually matching with the incoming customer_id.",
    });
    return;
  }

  await TeamsModel.create({
    name,
    customer_id,
  })
    .then((response) => {
      if (response) {
        res.status(httpStatusCode.CREATED).json({
          ok: true,
          message: "team was added successfully",
          team: response,
        });
      }
    })
    .catch((error) => {
      res
        .status(httpStatusCode.NOT_FOUND)
        .json({ ok: false, error: error.message });
      throw new Error(
        `Invalid customer data... see error : ${error.message}`.red.bgYellow
      );
    });
  res.end();
};

const getTeams = async (req, res, next) => {
  const { customer_id } = req.body;

  if (!customer_id) {
    res
      .status(httpStatusCode.BAD_REQUEST)
      .json({ error: "Please add customer_id field" });
    throw new Error("Please add customer_id field".red.bgYellow);
  } else if (!ObjectId.isValid(customer_id)) {
    res
      .status(httpStatusCode.BAD_REQUEST)
      .json({ error: "Please add a valid customer_id" });
    throw new Error("Please add a valid customer_id".red.bgYellow);
  }

  const teams = await TeamsModel.find({ customer_id });

  if (teams.length > 0) {
    res.status(httpStatusCode.OK).json({ ok: true, teams });
  } else {
    console.error(`Teams not found. ${teams}`.red.bgYellow);
    res.status(httpStatusCode.NOT_FOUND).json({
      ok: false,
      message: "Teams not found.",
    });
  }
  res.end();
};

module.exports = {
  addNewTeam,
  getTeams,
};
