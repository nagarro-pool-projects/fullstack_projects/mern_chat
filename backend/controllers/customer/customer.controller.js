const CustomerModel = require("../../models/customer/customers.model");
const httpStatusCode = require("../../common/httpStatusCode");
// const asyncHandler = require("express-async-handler");

const addNewCustomer = async (req, res, next) => {
  const { name } = req.body;
  if (!name) {
    res
      .status(httpStatusCode.BAD_REQUEST)
      .json({ error: "Please add the name field" });
    throw new Error("Please add the name field".red.bgYellow);
  }

  //Check if customer already exist
  const customerAlreadyExist = await CustomerModel.findOne({ name });
  if (customerAlreadyExist) {
    console.error("Customer already exist.".red.bgYellow);
    res
      .status(httpStatusCode.BAD_REQUEST)
      .json({ ok: false, error: "Customer already exist." });
    return;
  }

  await CustomerModel.create({
    name,
  })
    .then((response) => {
      if (response) {
        res.status(httpStatusCode.CREATED).json({
          ok: true,
          message: "customer was added successfully",
          customer: response,
        });
      }
    })
    .catch((error) => {
      res
        .status(httpStatusCode.NOT_FOUND)
        .json({ ok: false, error: error.message });
      throw new Error(
        `Invalid customer data... see error : ${error.message}`.red.bgYellow
      );
    });
  res.end();
};

const getCustomers = async (req, res, next) => {
  const allCustomers = await CustomerModel.find();
  if (allCustomers.length > 0) {
    res.status(httpStatusCode.OK).json({ ok: true, customers: allCustomers });
  } else {
    console.error(`Customers not found. ${allCustomers}`.red.bgYellow);
    res.status(httpStatusCode.NOT_FOUND).json({
      ok: false,
      message: "Customers not found.",
    });
  }
  res.end();
};

module.exports = {
  addNewCustomer,
  getCustomers,
};
