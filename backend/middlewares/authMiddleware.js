const jwt = require("jsonwebtoken")
const asyncHandler = require("express-async-handler")
const User = require("../models/user.model")

const protect = asyncHandler(async (req, res, next) => {
	let token

	//if (req.headers.authorization && req.headers.authorization.startsWith("Bearer")) {
	if (req.headers["x-auth-token"]) {
		try {
			//Get token form header
			//token = req.headers.authorization.split(" ")[1]
			token = req.headers["x-auth-token"]

			//Verify token
			const decoded = jwt.verify(token, process.env.JWT_SECRET)

			//Get user from token
			req.user = await User.findById(decoded.id).select("-password")

			next()
		} catch (err) {
			console.log(err.red)

			res.status(401).json({ error: "Not Authorized, user token has expired..." })
			throw new Error("Not Authorized, user token has expired...")
		}
	}

	if (!token) {
		res.status(401).json({ error: "Not Authorized, no token found..." })
		throw new Error("Not authorized, no token found...".red.bold)
	}
})

module.exports = {
	protect,
}
