const mongoose = require("mongoose");
// const messagesSchema = require("./message.model");
const { Types } = mongoose.Schema;

const chatsSchema = mongoose.Schema(
	{
		owner_id: {
			type: Types.ObjectId,
			required: [true, "owner_id is a required field"],
		},
		reciever_id: {
			type: Types.ObjectId,
			required: [true, "reciever_id is a required field"],
		},
		conversation: [
			{
				type: mongoose.Schema.Types.ObjectId,
				ref: "messageModel",
			},
		],
	},
	{
		minimize: false,
		timestamps: true,
		collection: "direct_chats",
	}
);

module.exports = mongoose.model("DirectChatModel", chatsSchema);
