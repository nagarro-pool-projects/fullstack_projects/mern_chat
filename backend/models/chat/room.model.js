const mongoose = require("mongoose");
const { Types } = mongoose.Schema;

const roomSchema = mongoose.Schema(
  {
    members: [
      {
        userName: {
          type: String,
          required: [true, "userName is a required field"],
        },
        UID: { type: Types.ObjectId, required: true },
      },
    ],
    room_name: {
      type: String,
      required: [true, "room_name is a required field"],
    },
    room_picture_path: { type: String },
    room_description: { type: String, default: "welcome to the amazing room" },
    owner_id: {
      type: Types.ObjectId,
      required: [true, "owner_id is a required id"],
    },
  },
  {
    minimize: false,
    timestamps: true,
    collection: "rooms",
  }
);

module.exports = mongoose.model("roomsModel", roomSchema);
