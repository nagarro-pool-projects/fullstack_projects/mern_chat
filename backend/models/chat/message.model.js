const mongoose = require("mongoose");
const { Types } = mongoose.Schema;

const messagesSchema = mongoose.Schema(
  {
    sender_id: {
      type: Types.ObjectId,
      required: [true, "owner_id is a required field"],
    },
    specific_receivers_ids: [
      {
        type: Types.ObjectId,
      },
    ],
    // direct_chat_id: {
    //   type: Types.ObjectId,
    //   required: [true, "direct_chat_id is a required field"],
    // },
    // room_id: {
    //   type: Types.ObjectId,
    //   required: [true, "room_id is a required field"],
    // },
    text: {
      type: String,
      required: [true, "text is a required field"],
    },
    attached_files_path: [
      {
        type: String,
      },
    ],
    thread_id: {
      type: Types.ObjectId,
    },
    //optional
    type_of_message: {
      // could be like : non, bug, refactorign , doubt, learning
      type: String,
    },
  },
  {
    minimize: true,
    timestamps: true,
    // collection: "messages",
  }
);

module.exports = mongoose.model("messageModel", messagesSchema);
// module.exports = messagesSchema;
