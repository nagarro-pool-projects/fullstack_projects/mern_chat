const mongoose = require("mongoose");
const { isEmail } = require("validator");
const { Types } = mongoose.Schema;

const userSchema = mongoose.Schema(
  {
    username: {
      type: String,
      required: [true, "username cannot go blank"],
      unique: true,
    },
    first_name: {
      type: String,
      required: [true, "first_name cannot go blank"],
    },
    last_name: {
      type: String,
      required: [true, "last_name cannot go blank"],
    },
    email: {
      type: String,
      unique: true,
      required: [true, "Email cannot go blank"],
      validate: [isEmail, "Invalid email syntax.."],
    },
    password: {
      type: String,
      required: [true, "Password cannot be empty"],
    },
    profile_picture_path: {
      type: String,
      default: "/",
    },
    online: {
      type: Boolean,
      default: false,
    },
    customer_id: {
      type: Types.ObjectId,
      unique: true,
      required: false,
    },
    team_id: {
      type: Types.ObjectId,
      unique: true,
      required: false,
    },
  },
  {
    minimize: false,
    timestamps: true,
    lowercase: true,
    collection: "users",
  }
);

module.exports = mongoose.model("User", userSchema);
