const mongoose = require("mongoose");
const { Types } = mongoose.Schema;
const teamsSchema = mongoose.Schema(
  {
    name: {
      type: String,
      required: [true, "name is a required data"],
    },
    customer_id: {
      type: Types.ObjectId,
      required: [true, "customer_id is a required data"],
    },
  },
  {
    minimize: false,
    timestamps: true,
    lowercase: true,
    collection: "teams",
  }
);

module.exports = mongoose.model("TeamsModel", teamsSchema);
