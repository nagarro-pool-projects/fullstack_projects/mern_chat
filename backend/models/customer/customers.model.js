const mongoose = require("mongoose");

const customerSchema = mongoose.Schema(
  {
    name: {
      type: String,
      required: [true, "customer name is a required data"],
      unique: true,
    },
  },
  {
    minimize: false,
    timestamps: true,
    lowercase: true,
    collection: "customers",
  }
);

module.exports = mongoose.model("CustomerModel", customerSchema);
