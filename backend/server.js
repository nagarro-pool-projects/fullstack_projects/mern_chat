const express = require("express");
const dotenv = require("dotenv").config();
const colors = require("colors");
const cors = require("cors");
const connectDB = require("./dbconn");
const PORT = process.env.PORT || 5000;
const app = express();
const bodyParser = require("body-parser");
// imported routes
const registerRoutes = require("./routes/user/register.routes");
const loginRoutes = require("./routes/user/login.routes");
const userAvailableRoutes = require("./routes/user/usernameAvailable.routes");
const customersRoutes = require("./routes/customers/customers.routes");
const teamsRoutes = require("./routes/customers/teams.routes");

connectDB();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cors());

// routes
app.use("/auth", loginRoutes, registerRoutes, userAvailableRoutes);
app.use("/pictures", express.static("pictures")); // folder which will be public
app.use("/customers", customersRoutes, teamsRoutes);

app.listen(PORT, () => {
  console.log(`Server started on port ${PORT}`);
});
