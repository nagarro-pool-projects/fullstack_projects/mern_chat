const express = require("express");
const router = express.Router();
const controller = require("../../controllers/user/register.controller");
const controllerUpload = require('../../controllers/multer.controller');


router.post("/register", controllerUpload.upload, controller.registerUser);


module.exports = router;
