const express = require("express")
const router = express.Router()
const controller = require("../../controllers/user/login.controller")
const { protect } = require("../../middlewares/authMiddleware")

router.post("/login", controller.logIn)
router.get("/getlogeduser", controller.getLogedUser)

module.exports = router
