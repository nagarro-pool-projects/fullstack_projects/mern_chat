const express = require("express");
const controller = require("../../controllers/user/usernameAvailable.controller");

const router = express.Router();
router.post("/usernameAvailable", controller.usernameAvailable);

module.exports = router;
