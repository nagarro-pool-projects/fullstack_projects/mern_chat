const express = require("express");
const router = express.Router();
const controller = require("../../controllers/customer/customer.controller");

router.post("/addNewCustomer", controller.addNewCustomer);
router.get("/getCustomers", controller.getCustomers);

module.exports = router;
