const express = require("express");
const router = express.Router();
const controller = require("../../controllers/customer/teams.controller");

router.post("/addNewTeam", controller.addNewTeam);
router.post("/getTeams", controller.getTeams);

module.exports = router;
