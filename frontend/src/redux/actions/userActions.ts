import { IInfoUser } from "../../interfaces/userInterfaces"
import { USER_AUTH_FAIL, USER_AUTH_SUCCESS, USER_LOADED } from "../constants/userConstants"

export const loginUser = (user: IInfoUser) => {
	return {
		type: USER_AUTH_SUCCESS,
		payload: user,
	}
}

export const loadUser = (user: IInfoUser) => {
	return {
		type: USER_LOADED,
		payload: user,
	}
}

export const clearUser = (error: string) => {
	return {
		type: USER_AUTH_FAIL,
		payload: error,
	}
}
