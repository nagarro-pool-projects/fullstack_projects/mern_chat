import { IStandardAction } from "../../interfaces/payloadInterfaces"
import { IInfoUser } from "../../interfaces/userInterfaces"
import { USER_AUTH_FAIL, USER_AUTH_SUCCESS, USER_LOADED } from "../constants/userConstants"

export interface IInitalState {
	token: string | null
	isAuthenticated: boolean
	isLoading: boolean
	user: IInfoUser
	error: null | { msg: string }
}
const initialState: IInitalState = {
	token: localStorage.getItem("token") || "",
	isAuthenticated: false,
	isLoading: false,
	user: {
		username: "",
		last_name: "",
		first_name: "",
		id: "",
		token: "",
		email: "",
		profile_picture_path: "/",
		online: false,
	},
	error: null,
}

const userReducer = (state = initialState, action: IStandardAction) => {
	switch (action.type) {
		case USER_LOADED:
			return {
				...state,
				token: state.token,
				user: {
					username: action.payload && action.payload.username,
					last_name: action.payload?.last_name,
					first_name: action.payload?.first_name,
					id: action.payload?.id,
					token: action.payload?.token,
					email: action.payload?.email,
					profile_picture_path: action.payload?.profile_picture_path,
					online: true,
				},
				isAuthenticated: true,
				isLoading: false,
				error: null,
			}

		case USER_AUTH_SUCCESS:
			if (action.payload?.token) localStorage.setItem("token", action.payload.token)

			return {
				...state,
				token: action.payload?.token,
				user: {
					username: action.payload && action.payload.username,
					last_name: action.payload?.last_name,
					first_name: action.payload?.first_name,
					id: action.payload?.id,
					token: action.payload?.token,
					email: action.payload?.email,
					profile_picture_path: action.payload?.profile_picture_path,
					online: true,
				},
				isAuthenticated: true,
				isLoading: false,
				error: null,
			}
		case USER_AUTH_FAIL:
			localStorage.removeItem("token")

			return {
				...state,
				token: "",
				isAuthenticated: false,
				isLoading: false,
				user: null,
				error: action.payload,
			}
		default:
			return state
	}
}

export default userReducer
