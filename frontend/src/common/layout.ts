// eslint-disable-next-line import/no-anonymous-default-export
export default {
	colors: {
		green: "#47D7AC", // main
		greenLight: "#E7FAF4", // main semi transparent
		darkBlue: "#13294B", //secondary
		purple: "#7378EE", // decorator
		withe: "#FFFFFF", // base
		black: "#333333",
		grayLight: "#f3f3f3",
	},
	backgrounds: {
		darkMode: {},
		lightMode: {},
	},
	fonts: {
		darkMode: {},
		lightMode: {
			white: "#FFFFFF",
			gray: "#817E7E",
			grayTitle: "#606060",
		},
		family: {
			inter: `'Inter', sans-serif`,
		},
	},
	icons: {
		colors: {
			active: "#D9D9D9",
		},
	},
	globalPaddings: {
		left: "60px",
		right: "60px",
	},
}
