const validRegexEmail =
  /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/;

  /**
   * check if email is a valid email and returns true, otherwise false.
   * @param incomingEmail should be and email string like hellyeah@gmail.com
   */
export const validEmail = async (incomingEmail: string): Promise<boolean> => {
  if (incomingEmail.match(validRegexEmail)) return true;
  return false;
};


  /**
   * inserrt localstorage.
   * @param referenceName should be and email string like hellyeah@gmail.com
   * @param value should be and email string like hellyeah@gmail.com
   */
const setLocalStorage = (referenceName:string, value:any)=>{

};
