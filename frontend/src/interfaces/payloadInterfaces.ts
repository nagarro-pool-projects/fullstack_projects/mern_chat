import { IInfoUser } from "./userInterfaces"

export interface IStandardAction {
	type: string
	payload?: IInfoUser
}
