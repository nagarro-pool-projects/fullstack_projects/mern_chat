import { IInitalState } from "../redux/reducers/userReducer"

export interface IlogUser {
	email: string
	password: string
	remember?: boolean
	token?: string
}

export interface IInfoUser {
	id: string
	username: string
	first_name: string
	last_name: string
	email: string
	profile_picture_path: string
	online: boolean
	token: string
}

export interface IUserStore {
	userReducer: IInitalState
}
