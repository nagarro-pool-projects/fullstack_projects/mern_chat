import { createGlobalStyle } from "styled-components";
import layout from "./common/layout";

export const IndexStyle = createGlobalStyle`
  * {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
	  font-family: ${layout.fonts.family.inter};
    text-decoration:none;
    list-style:none;

    .labelRoot{
      color:${layout.fonts.lightMode.gray}!important;
    }
    .labelFocused{
      color:${layout.colors.darkBlue}!important;
    }

    .inputRoot{
      margin-bottom:30px;
      color:${layout.colors.darkBlue}!important;
      border-bottom: 2px solid ${layout.colors.green} !important;
    }

    .inputFocused{
      border-bottom: 2px solid ${layout.colors.darkBlue} !important;
    }

    .goback{
      color: ${layout.colors.purple}
    }

    input:-webkit-autofill,
input:-webkit-autofill:hover, 
input:-webkit-autofill:focus, 
input:-webkit-autofill:active{
    -webkit-box-shadow: 0 0 0 30px white inset !important;
}

  }
`;
