import {BrowserRouter as Router} from 'react-router-dom';
import MainNavigation from './navigation/main.navigation';

function App() {
	return (
		<Router>
			<MainNavigation />
		</Router>
	)
}

export default App
