import { Route, Routes } from "react-router-dom"
import Contacts from "../../components/authenticated/contacts/contacts"
import Chat from "../../components/authenticated/chat/chat"
import Home from "../../components/authenticated/home/home"
import Settings from "../../components/authenticated/settings/settings"
import Rooms from "../../components/authenticated/rooms/rooms"

const AuthenticatedNavigation = () => {
	return (
		<Routes>
			<Route path="/home" element={<Home />} />
			<Route path="/chat" element={<Chat />} />
			<Route path="/contacts" element={<Contacts />} />
			<Route path="/rooms" element={<Rooms />} />
			<Route path="/settings" element={<Settings />} />
		</Routes>
	)
}

export default AuthenticatedNavigation
