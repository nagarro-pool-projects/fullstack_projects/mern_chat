import LeftNavSection from "../../../components/authenticated/LeftNavSection/LeftNavSection"
import AuthenticatedNavigation from "../authenticated.navigation"
import { MainContainer } from "./mainContainer.style"

const AuthenticatedUI = () => {
	return (
		<MainContainer>
			<div style={{ display: "flex", flex: 0.1 }}>
				<LeftNavSection />
			</div>
			<div style={{ flex: 0.9, padding: "28px 12px" }}>
				<AuthenticatedNavigation />
			</div>
		</MainContainer>
	)
}

export default AuthenticatedUI
