import { Route, Routes } from "react-router-dom"
import GetStarted from "../../components/unauthenticated/getStarted/getStarted"
import Register from "../../components/unauthenticated/register/register"
import Login from "../../components/unauthenticated/login/login"

const UnauthNavigation = () => {
	return (
		<Routes>
			<Route path="/" element={<GetStarted />} />
			<Route path="/register" element={<Register />} />
			<Route path="/login" element={<Login />} />
		</Routes>
	)
}

export default UnauthNavigation
