import axios from "axios"
import { FC, useEffect } from "react"
import { useDispatch, useSelector } from "react-redux"
import { useNavigate } from "react-router-dom"
import endpoints from "../apis/endpoints"
import { IUserStore } from "../interfaces/userInterfaces"
import { clearUser, loadUser } from "../redux/actions/userActions"
import { IInitalState } from "../redux/reducers/userReducer"
import AuthenticatedUI from "./authenticated/MainWindow/authenticated.UI"
import UnauthNavigation from "./unauthenticated/unauthenticated.navigation"

const MainNavigation: FC = () => {
	const user: IInitalState = useSelector((state: IUserStore) => state.userReducer)

	const dispatch = useDispatch()
	const navigate = useNavigate()

	useEffect(() => {
		if (user.token) {
			if (user.isAuthenticated) {
				navigate("/home")
			} else {
				checkToken()
			}
		} else {
			navigate("/")
		}
	}, [user])

	const checkToken = () => {
		const config = {
			headers: {
				"Content-Type": "application/json",
				"x-auth-token": user.token,
			},
		}
		axios
			.get(endpoints.loadUser, config)
			.then((res) => {
				dispatch(loadUser(res.data))
			})
			.catch((err) => {
				console.log("Some error on backend", err)
				dispatch(clearUser(err.response.data.error))
			})
	}

	return <>{user.token ? <AuthenticatedUI /> : <UnauthNavigation />}</>
}

export default MainNavigation
