import styled from "styled-components";
import bg from '../../../assets/images/register_login/bg.png';
import layout from "../../../common/layout";

export const MainContainer = styled.div`
  width: 100%;
  height: 100vh;
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  background-image: url(${bg});
`;

export const DataContainer = styled.div`
    position:absolute;
    top:0px;
    left:0px;
    bottom:0px;
    width:50%;
    display:flex;
    flex-direction:column;

    .titleContainer{
        flex:0.4;
        padding-bottom:100px;
        padding-left:${layout.globalPaddings.left};
        display:flex;
        justify-content:flex-end;
        flex-direction: column;

        .title{
            margin-top:20px;
            font-size:42px;
            font-weight:500;
            color:${layout.fonts.lightMode.grayTitle};
            letter-spacing:1px;
        }

        .subtitle{
            margin-top:20px;
            font-size:20px;
            font-weight:200;
            color:${layout.fonts.lightMode.gray};
            letter-spacing:1px;
        }
    }

    .formContainer{
        flex:0.6;
        display:flex;
        padding-left:${layout.globalPaddings.left};

        .form{
            width:50%;
        }

        .button{
            margin-top:20px;
            padding:15px 65px;
            background-color:${layout.colors.green};
            border:none;
            border-radius:50px;
            cursor: pointer;
            transition:0.5s;
            font-size: 20px;
        }

        .button:hover{
            background-color:${layout.colors.darkBlue};
            color:${layout.fonts.lightMode.white};
            transition:0.5s;
        }
    }

`;

