import { Link } from "react-router-dom";
import { MainContainer, DataContainer } from "./login.style";
import TextField from "@mui/material/TextField";
import VisibilityIcon from "@mui/icons-material/Visibility";
import InputAdornment from "@mui/material/InputAdornment";
import IconButton from "@mui/material/IconButton";

const LoginUI = (props?: any) => {
  const { handleLogin, handleChange, user, handleShowPassword, showPassword } =
    props;
  return (
    <MainContainer>
      <DataContainer>
        <div className="titleContainer">
          <Link to="/">
            <p className="goback">Go back</p>
          </Link>
          <p className="title" data-testid="login_title">
            Sign in
          </p>
          <p className="subtitle">
            Welcome back! we're happy to see you here again.
          </p>
        </div>
        <div className="formContainer">
          <form className="formLogin" onSubmit={handleLogin}>
            {/* NAGARRO EMAIL */}
            <TextField
              id="email"
              name="email"
              label="Email"
              variant="standard"
              fullWidth={true}
              type="email"
              InputLabelProps={{
                classes: {
                  root: "labelRoot",
                  focused: "labelFocused",
                },
              }}
              InputProps={{
                disableUnderline: true,
                classes: {
                  root: "inputRoot",
                  focused: "inputFocused",
                },
              }}
              onChange={(e) => handleChange(e.target)}
            />
            {/* PASSWORD */}
            <TextField
              id="password"
              name="password"
              label="Password"
              variant="standard"
              fullWidth={true}
              type={showPassword ? "text" : "password"}
              InputLabelProps={{
                classes: {
                  root: "labelRoot",
                  focused: "labelFocused",
                },
              }}
              InputProps={{
                disableUnderline: true,
                classes: {
                  root: "inputRoot",
                  focused: "inputFocused",
                },
                endAdornment: (
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="toggle password visibility"
                      onMouseDown={() => handleShowPassword()}
                      onMouseUp={() => handleShowPassword()}
                    >
                      <VisibilityIcon />
                    </IconButton>
                  </InputAdornment>
                ),
              }}
              onChange={(e) => handleChange(e.target)}
            />
            <button
              type="submit"
              className="button"
              disabled={!user.email || !user.password}
            >
              Sign in
            </button>
          </form>
        </div>
      </DataContainer>
    </MainContainer>
  );
};

export default LoginUI;
