import axios from "axios"
import { useState, FC } from "react"
import { useDispatch } from "react-redux"
import endpoints from "../../../apis/endpoints"
import headerConfig from "../../../apis/header.config"
import { validEmail } from "../../../common/helpers"
import { loginUser } from "../../../redux/actions/userActions"
import LoginUI from "./login.UI"

interface ILogin {
	email: string
	password: string
}
const Login: FC = () => {
	const [user, setUser] = useState<ILogin>({
		email: "",
		password: "",
	})

	const [showPassword, setShowPassword] = useState(false)
	const dispatch = useDispatch()

	const handleShowPassword = () => {
		setShowPassword(!showPassword)
	}
	/**
	 * UPDATE THE STATE VARIABLE OF COMPONENT (email, password)
	 * @param eventTarget should be an event.target
	 */
	const handleChange = async (eventTarget: EventTarget & (HTMLInputElement | HTMLTextAreaElement)) => {
		let validatedEmail: boolean = false
		if (eventTarget.name === "email" && (await validEmail(eventTarget.value))) {
			validatedEmail = true
		}
		setUser({
			...user,
			[eventTarget.name]: eventTarget.name === "email" ? validatedEmail && eventTarget.value : eventTarget.value,
		})
	}

	/**
	 * LOGIN ENDPOINT CALL
	 *
	 */
	const handleLogin = (event: any) => {
		event.preventDefault()
		axios
			.post(endpoints.login, JSON.stringify(user), headerConfig.headerAppJson)
			.then((res) => {
				dispatch(loginUser(res.data))
			})
			.catch((err) => {
				console.log("axios endpoint error", err.response.data)
			})
	}

	return (
		<LoginUI
			handleChange={handleChange}
			handleLogin={handleLogin}
			handleShowPassword={handleShowPassword}
			showPassword={showPassword}
			user={user}
		/>
	)
}

export default Login
