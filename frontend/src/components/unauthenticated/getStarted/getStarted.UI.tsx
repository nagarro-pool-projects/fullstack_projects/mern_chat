import {
  MainImageContainer,
  Overlay,
  NavigationAndContentContainer,
  NavigationChildContainer,
  TextAndButtonChildContainer,
} from "./getStarted.style";
import logoCroped from "../../../assets/images/getStarted/logo.png";
import logo from "../../../assets/images/common/logo.png";
import { Link } from "react-router-dom";

const GetStartedUI = () => {
  return (
    <MainImageContainer data-testid="bgMainImage">
      <Overlay />
      <img src={logoCroped} className="logoRotated" alt="logoCroped" />
      <NavigationAndContentContainer>
        <NavigationChildContainer>
          <div className="logoContainer">
            <img className="logo" src={logo} alt="" />
            <p>eiit</p>
          </div>
          <nav className="navigationContainer">
            <li>
              <Link to="/login" data-testid="login_button">
                <p>Sign in</p>
              </Link>
            </li>
          </nav>
        </NavigationChildContainer>
        <TextAndButtonChildContainer>
          <div className="textContainer">
            <p className="textStyle">
              GET HELP FROM THOUSANDS OF NAGARRIANS DEVS AROUND THE WORLD.
            </p>
          </div>
          <div className="buttonContainer">
            <Link className="button" to="/register">
              Sign up
            </Link>
          </div>
        </TextAndButtonChildContainer>
      </NavigationAndContentContainer>
    </MainImageContainer>
  );
};

export default GetStartedUI;
