import {
  fireEvent,
  prettyDOM,
  render,
  screen,
  waitFor,
} from "@testing-library/react";
import GetStarted from "../getStarted";
import { BrowserRouter } from "react-router-dom";
import layout from "../../../../common/layout";
import Login from "../../login/login";

describe("CHECK THE CORRECT NAVIGATION AND STYLE OF BUTTONS (LOGIN) AND (SIGN IN)", () => {
  beforeEach(async () => {
    render(<GetStarted />, { wrapper: BrowserRouter });
  });
  it.only("should check if (login) button has the right initial styles and hover", async () => {
    const loginButton = screen.getByTestId("login_button");
    expect(loginButton).toBeInTheDocument();
    // getting tag P that comes from loggin Buton
    const nestedLoginButtonElement: any = loginButton.firstChild;
    // expect(loginButton.find("a:first-child > p").text()).toBe("Sign in")
    // console.log(prettyDOM(nestedLoginButtonElement));
    expect(nestedLoginButtonElement).toHaveStyle({
      borderBottom: `3px solid ${layout.colors.green}`,
    });
  });
});
