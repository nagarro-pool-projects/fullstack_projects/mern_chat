import { prettyDOM, render, screen } from "@testing-library/react";
import GetStarted from "../getStarted";
import { BrowserRouter } from "react-router-dom";

describe("CHECK THE INITIAL ELEMENTS", () => {
  beforeEach(() => {});
  test("button login should be enable and should redirect to login page", () => {
    render(<GetStarted />, { wrapper: BrowserRouter });
    const image = screen.getByTestId("bgMainImage");
    // console.log(prettyDOM(image));
    expect(image).toBeInTheDocument();
  });
});
