import styled from "styled-components";
import mainBg from "../../../assets/images/getStarted/mainBg.jpg";
import layout from "../../../common/layout";

export const MainImageContainer = styled.div`
  width: 100%;
  height: 100vh;
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  background-image: url(${mainBg});

  .logoRotated {
    position: absolute;
    right: 0px;
    bottom: 0px;
    z-index: 1;
  }
`;

export const Overlay = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: black;
  opacity: 0.45;
`;

export const NavigationAndContentContainer = styled.div`
  display: flex;
  height: 100vh;
  flex-direction: column;
`;

export const NavigationChildContainer = styled.div`
  display: flex;
  flex: 0.12;
  padding: 0px ${layout.globalPaddings.right};
  z-index: 1;

  .logoContainer {
    display: flex;
    flex-direction: column;
    flex: 0.1;
    justify-content: flex-end;

    .logo {
      width: 60px;
      height: fit-content;
    }

    p{
        color:${layout.colors.withe};
        padding:5px 0px 0px 16px;
        letter-spacing:1px;
    }
  }

  .navigationContainer {
    display: flex;
    flex: 0.9;

    li{
        display:flex;
        align-items: flex-end;
        justify-content:flex-end;
        flex:1;

        p{
            color:${layout.colors.withe};
            padding-bottom:3px;
            border-bottom: 3px solid ${layout.colors.green};
            transition:0.5s;
        }

        p:hover{
            border-bottom: 3px solid ${layout.colors.greenLight};
            transition:0.5s;
        }
    }
  }
`;

export const TextAndButtonChildContainer = styled.div`
  display: flex;
  flex: 0.88;
  flex-direction: column;

  .textContainer {
    display: flex;
    flex: 0.8;
    padding: 0px 0px 40px ${layout.globalPaddings.left};

    .textStyle {
      flex: 0.7;
      align-self: flex-end;
      font-size: 45px;
      z-index: 1;
      letter-spacing: 2px;
      font-weight: 300;
      color: ${layout.colors.withe};
    }
  }

  .buttonContainer {
    display: flex;
    flex: 0.2;
    padding-left: ${layout.globalPaddings.left};

    .button {
      z-index: 1;
      padding:15px 65px;
      background-color: ${layout.colors.green};
      height: fit-content;
      border-radius: 50px;
      color: ${layout.colors.black};
      transition:0.5s;
      font-size: 20px;
    }
    .button:hover{
        background-color: ${layout.colors.darkBlue};
        color:${layout.colors.withe};
        transition:0.5s;
    }
  }
`;
