import { FC, useState, useEffect } from "react";
import GetStartedUI from "./getStarted.UI";

const GetStarted: FC = () => {
  return <GetStartedUI/>;
};

export default GetStarted;
