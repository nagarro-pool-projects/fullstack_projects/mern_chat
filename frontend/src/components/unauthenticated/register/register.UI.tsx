import { Link } from "react-router-dom";
import { MainContainer, DataContainer } from "./register.style";
import TextField from "@mui/material/TextField";
import VisibilityIcon from "@mui/icons-material/Visibility";
import InputAdornment from "@mui/material/InputAdornment";
import IconButton from "@mui/material/IconButton";

const RegisterUI = (props?: any) => {
  const {
    user,
    handleChanges,
    submitRegisterUser,
    handleShowPassword,
    showPassword,
    passwordsMatching,
    passwordsMatched
  } = props;
  return (
    <MainContainer>
      <DataContainer>
        <div className="titleContainer">
          <Link to="/">
            <p className="goback">Go back</p>
          </Link>
          <p className="title">Let’s reach the sky together.</p>
          <p className="subtitle">
            Create your account to resolve doubts and give a hand <br /> about
            programing topics.
          </p>
        </div>
        <div className="formContainer">
          <form
            action="register"
            className="form"
            onSubmit={submitRegisterUser}
          >
            {/* FIRST NAME INPUT */}
            <TextField
              name="first_name"
              id="first_name"
              label="First Name"
              variant="standard"
              fullWidth={true}
              type="text"
              InputLabelProps={{
                classes: {
                  root: "labelRoot",
                  focused: "labelFocused",
                },
              }}
              InputProps={{
                disableUnderline: true,
                classes: {
                  root: "inputRoot",
                  focused: "inputFocused",
                },
              }}
              onChange={(e) => {
                handleChanges(e.target);
              }}
            />
            {/* LAST NAME */}
            <TextField
              name="last_name"
              id="last_name"
              label="Last Name"
              variant="standard"
              fullWidth={true}
              type="text"
              InputLabelProps={{
                classes: {
                  root: "labelRoot",
                  focused: "labelFocused",
                },
              }}
              InputProps={{
                disableUnderline: true,
                classes: {
                  root: "inputRoot",
                  focused: "inputFocused",
                },
              }}
              onChange={(e) => {
                handleChanges(e.target);
              }}
            />
            {/* NICKNAME */}
            <TextField
              name="username"
              id="username"
              label="Nickname"
              variant="standard"
              fullWidth={true}
              type="text"
              InputLabelProps={{
                classes: {
                  root: "labelRoot",
                  focused: "labelFocused",
                },
              }}
              InputProps={{
                disableUnderline: true,
                classes: {
                  root: "inputRoot",
                  focused: "inputFocused",
                },
              }}
              onChange={(e) => {
                handleChanges(e.target);
              }}
            />
            {/* NAGARRO EMAIL */}
            <TextField
              name="email"
              id="email"
              label="Nagarro email"
              variant="standard"
              fullWidth={true}
              type="email"
              InputLabelProps={{
                classes: {
                  root: "labelRoot",
                  focused: "labelFocused",
                },
              }}
              InputProps={{
                disableUnderline: true,
                classes: {
                  root: "inputRoot",
                  focused: "inputFocused",
                },
              }}
              onChange={(e) => {
                handleChanges(e.target);
              }}
            />
            {!passwordsMatched && <p className="passwordErrorMessage">
              Password doesnt match with confirm password
            </p>}
            {/* PASSWORD */}
            <TextField
              name="password"
              id="password"
              label="Password"
              variant="standard"
              fullWidth={true}
              type={showPassword ? "text" : "password"}
              InputLabelProps={{
                classes: {
                  root: "labelRoot",
                  focused: "labelFocused",
                },
              }}
              InputProps={{
                disableUnderline: true,
                classes: {
                  root: "inputRoot",
                  focused: "inputFocused",
                },
                endAdornment: (
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="toggle password visibility"
                      onMouseDown={() => handleShowPassword()}
                      onMouseUp={() => handleShowPassword()}
                    >
                      <VisibilityIcon />
                    </IconButton>
                  </InputAdornment>
                ),
              }}
              onChange={(e) => {
                handleChanges(e.target);
                passwordsMatching(e.target);
              }}
            />
            {/* CONFIRM PASSWORD */}
            <TextField
              name="confirmPassword"
              id="confirmPassword"
              label="Confirm password"
              variant="standard"
              fullWidth={true}
              type={showPassword ? "text" : "password"}
              InputLabelProps={{
                classes: {
                  root: "labelRoot",
                  focused: "labelFocused",
                },
              }}
              InputProps={{
                disableUnderline: true,
                classes: {
                  root: "inputRoot",
                  focused: "inputFocused",
                },
                endAdornment: (
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="toggle password visibility"
                      onMouseDown={() => handleShowPassword()}
                      onMouseUp={() => handleShowPassword()}
                    >
                      <VisibilityIcon />
                    </IconButton>
                  </InputAdornment>
                ),
              }}
              onChange={(e) => {
                handleChanges(e.target);
                passwordsMatching(e.target);
            }}
            />
            <button
              type="submit"
              className="button"
              disabled={
                !user.first_name ||
                !user.last_name ||
                !user.email ||
                !user.username ||
                !user.password
              }
            >
              Sign up
            </button>
          </form>
        </div>
      </DataContainer>
    </MainContainer>
  );
};

export default RegisterUI;
