import { useState, useEffect } from "react";
import RegisterUI from "./register.UI";
import axios from "axios";
import endpoints from "../../../apis/endpoints";
import headerConfig from "../../../apis/header.config";
interface IUser {
  username: string;
  first_name: String;
  last_name: String;
  email: string;
  password: string;
  profile_picture_path: string;
  online: boolean;
}

const Register = () => {
  const [user, setUser] = useState<IUser>({
    username: "",
    first_name: "",
    last_name: "",
    email: "",
    password: "",
    profile_picture_path: "",
    online: true,
  });

  const [usernameAvailable, setUsernameAvailable] = useState(false);
  const [showPassword, setShowPassword] = useState(false);
  const [passwordsMatched, setPasswordsMatched] = useState(true);
  const [confirmPassword, setconfirmPassword] = useState("");

  useEffect(() => {
    console.log(`user  : ${JSON.stringify(user)}`);
  }, [user]);

  /**
   * UPDATE THE STATE OF COMPONENT (user)
   * @param eventTarget should be an event.target
   */
  const handleChanges = (eventTarget: any) => {
    setUser({ ...user, [eventTarget.name]: eventTarget.value });
    if(eventTarget.name === "confirmPassword"){
        setconfirmPassword(eventTarget.value);
    }
  };

  /**
   *  UPDATE THE STATE (showPassword)
   *
   */
  const handleShowPassword = () => {
    setShowPassword(!showPassword);
  };

  /**
   * check if passwords are matching and update the state (passwordsMatched)
   * @param eventTarget should be an input event.target
   */
  const passwordsMatching = (eventTarget: any) => {
    switch (eventTarget.name) {
      case "password":
        if (eventTarget.value === confirmPassword) {
          setPasswordsMatched(true);
        } else if(confirmPassword != ""){
          setPasswordsMatched(false);
        }
        break;

      case "confirmPassword":
        if (eventTarget.value === user.password) {
          setPasswordsMatched(true);
        } else if(user.password != ""){
          setPasswordsMatched(false);
        }
        break;
    }
  };

  /**
   * SUBMIT REGISTER NEW USER
   * @param event should be an input event
   */
  const submitRegisterUser = async (event: any) => {
    event.preventDefault();
    const userAvailable: boolean = await verifyUsernameAvailableEndPoint();
    if (userAvailable) {
      // alert here ->

      registerUserEndPoint();
    } else {
      console.log("failing to register user");
    }
  };

  /**
   * ENDPOINT TO VERIFY IF USERNAME(nickname) is available.
   *
   */
  const verifyUsernameAvailableEndPoint = async (): Promise<boolean> => {
    let available = false;
    await axios
      .post(
        endpoints.checkUserAvailability,
        JSON.stringify({ username: user.username }),
        headerConfig.headerAppJson
      )
      .then((res: any) => {
        if (res.data && res.data.ok) {
          // alert here =>
          available = true;
        } else if (res.data && !res.data.ok) {
          // alert here =>
          available = false;
        }
      })
      .catch((err) => {
        // alert here =>
        console.log("axios endpoint error", err.response.data);
        available = false;
      });
    return available;
  };

  /**
   * ENDPOINT TO REGISTER A NEW USER
   *
   */
  const registerUserEndPoint = async () => {
    return await axios
      .post(
        endpoints.register,
        JSON.stringify(user),
        headerConfig.headerAppJson
      )
      .then((res: any) => {
        // alert here =>
        // console.log(res.data.message);   //  message : created succesfully
        // console.log(res.status);  // shoul be 201
        // console.log(res.data.user.token);  // user token
      })
      .catch((err) => {
        console.log("axios register endpoint error", err.response.data);
        //
        // console.log(err.response.data.error)  // message : "User already exist."
        return false;
      });
  };

  return (
    <RegisterUI
      handleChanges={handleChanges}
      submitRegisterUser={submitRegisterUser}
      user={user}
      handleShowPassword={handleShowPassword}
      showPassword={showPassword}
      passwordsMatching={passwordsMatching}
      passwordsMatched={passwordsMatched}
    />
  );
};

export default Register;
