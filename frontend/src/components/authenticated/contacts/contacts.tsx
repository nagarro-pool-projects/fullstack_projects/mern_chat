import { FC, useEffect } from "react";
import { useSelector } from "react-redux";
import { IUserStore } from "../../../interfaces/userInterfaces";
import { IInitalState } from "../../../redux/reducers/userReducer";
import ContactsUI from "./contactsUI";

const Contacts: FC = () => {
	const userFromRedux: IInitalState = useSelector(
		(state: IUserStore) => state.userReducer
	);
	const { user } = userFromRedux;

	useEffect(() => {
		if (userFromRedux !== undefined) {
		}

		return () => {
			//  this is like componentDidUnmout
		};
	}, [userFromRedux]);

	return <ContactsUI user={user} />;
};

export default Contacts;
