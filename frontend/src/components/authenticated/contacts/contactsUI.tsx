import { IInfoUser } from "../../../interfaces/userInterfaces"
import { ContactsContainerStyle } from "./contacts.style"

interface userProps {
	user: IInfoUser
}

const ContactsUI = ({ user }: userProps) => {
	return (
		<ContactsContainerStyle>
			<div>This is contacts page</div>
		</ContactsContainerStyle>
	)
}

export default ContactsUI
