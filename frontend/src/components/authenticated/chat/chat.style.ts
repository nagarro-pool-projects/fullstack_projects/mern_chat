import styled from "styled-components"
import layout from "../../../common/layout"

export const MainChatContainer = styled.main`
	display: flex;
`

export const ChatSection = styled.section`
	display: flex;
	flex-direction: row;
	align-items: flex-start;
	background-color: ${layout.colors.withe};
	width: 100%;

	input {
		flex-grow: 2;
		padding: 18px 15px;

		border: none;
		border-radius: 10px;
		background-color: ${layout.colors.grayLight};
		font-size: large;
	}

	input:focus {
		background-color: ${layout.colors.grayLight};
		outline: none;
	}
`
