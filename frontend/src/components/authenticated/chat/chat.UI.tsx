import { IInfoUser } from "../../../interfaces/userInterfaces"
import UserInfo from "../userInfo/userInfo"
import { ChatSection } from "./chat.style"

const ChatUI = () => {
	return (
		<ChatSection>
			<input placeholder="Search by topic" />
			<UserInfo />
		</ChatSection>
	)
}

export default ChatUI
