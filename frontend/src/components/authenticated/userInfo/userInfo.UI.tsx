import { NavLink } from "react-router-dom"
import { UserInfoStyle } from "./userInfo.style"
import PersonIcon from "@mui/icons-material/Person"
import { IInfoUser } from "../../../interfaces/userInterfaces"

interface userProps {
	user: IInfoUser
}

const UserInfoUI = ({ user }: userProps) => {
	return (
		<UserInfoStyle>
			{`${user.first_name} ${user.last_name.substring(0, 1)}.`}
			<NavLink className="user-icon" to="/profile">
				<PersonIcon />
			</NavLink>
		</UserInfoStyle>
	)
}

export default UserInfoUI
