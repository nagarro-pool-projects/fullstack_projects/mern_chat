import { useSelector } from "react-redux"
import { IUserStore } from "../../../interfaces/userInterfaces"
import { IInitalState } from "../../../redux/reducers/userReducer"
import UserInfoUI from "./userInfo.UI"

const UserInfo = () => {
	const userFromRedux: IInitalState = useSelector((state: IUserStore) => state.userReducer)

	return <UserInfoUI user={userFromRedux.user} />
}

export default UserInfo
