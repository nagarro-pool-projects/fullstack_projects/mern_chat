import styled from "styled-components"
import layout from "../../../common/layout"

export const UserInfoStyle = styled.div`
	text-align: end;
	flex-grow: 1;
	padding: 9px 12px;

	.user-icon {
		padding: 16px 14px;
		margin: 10px;
		border: none;
		border-radius: 50px;
		cursor: pointer;
		color: ${layout.icons.colors.active};
		background-color: ${layout.fonts.lightMode.gray};
	}
`
