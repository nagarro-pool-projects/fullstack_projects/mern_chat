import UserInfo from "../userInfo/userInfo"
import { HomeStyle } from "./home.style"

const HomeUI = () => {
	return (
		<HomeStyle>
			<UserInfo />
			<div>Let's get you started, check your messages, ask new questions, or connect with people all over Nagarro!</div>
		</HomeStyle>
	)
}

export default HomeUI
