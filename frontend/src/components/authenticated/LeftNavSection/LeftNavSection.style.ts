import styled from "styled-components"
import layout from "../../../common/layout"

export const LeftNavSectionStyle = styled.section`
	display: flex;
	flex-direction: column;
	justify-content: space-between;
	background-color: ${layout.colors.darkBlue};
	padding-top: 42px;
	align-items: center;
	//min-width: 6vw;
	height: 100vh;

	.logo {
		width: 60px;
		padding-bottom: 42px;
		margin: 0 16px;
	}
	.button-container {
		display: flex;
		flex-direction: column;
	}

	.boxes {
		padding: 10px;
		margin: 16px 22.5px;
		border: none;
		border-radius: 10px;
		cursor: pointer;
		color: ${layout.icons.colors.active};
	}
	.boxes:hover {
		background-color: ${layout.colors.green};
	}

	.selected-box {
		background-color: ${layout.colors.green};
	}

	.button-config {
		border-radius: 50%;
	}
`
