import { Link, NavLink } from "react-router-dom"
import { LeftNavSectionStyle } from "./LeftNavSection.style"
import PersonIcon from "@mui/icons-material/Person"
import ForumIcon from "@mui/icons-material/Forum"
import BeachAccessIcon from "@mui/icons-material/BeachAccess"
import Diversity3Icon from "@mui/icons-material/Diversity3"
import SettingsIcon from "@mui/icons-material/Settings"
import logo from "../../../assets/images/common/logo.png"

const LeftNavSection = () => {
	return (
		<LeftNavSectionStyle>
			<div className="button-container">
				<Link to="home">
					<img className="logo" src={logo} alt="Lg" />
				</Link>
				<NavLink to="/chat" className={({ isActive }) => (isActive ? "boxes selected-box" : "boxes")}>
					<ForumIcon />
				</NavLink>
				<NavLink to="/contacts" className={({ isActive }) => (isActive ? "boxes selected-box" : "boxes")}>
					<PersonIcon />
				</NavLink>
				<NavLink to="/rooms" className={({ isActive }) => (isActive ? "boxes selected-box" : "boxes")}>
					<Diversity3Icon />
				</NavLink>
				<NavLink to="/something" className={({ isActive }) => (isActive ? "boxes selected-box" : "boxes")}>
					<BeachAccessIcon />
				</NavLink>
			</div>
			<NavLink to="/settings" className={({ isActive }) => (isActive ? "boxes selected-box" : "boxes")}>
				<SettingsIcon />
			</NavLink>
		</LeftNavSectionStyle>
	)
}

export default LeftNavSection
