const endpoints = {
	register: "http://localhost:5000/auth/register",
	login: "http://localhost:5000/auth/login",
	checkUserAvailability: "http://localhost:5000/auth/usernameAvailable",
	loadUser: "http://localhost:5000/auth/getlogeduser",
	logout: "fakeURL",
}

export default endpoints
