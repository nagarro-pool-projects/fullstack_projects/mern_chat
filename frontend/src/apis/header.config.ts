const headerConfig = {
	headerAppJson: {
		headers: {
			"Content-Type": "application/json",
		},
	},
	headerTokenJson: {
		headers: {
			"Content-Type": "application/json",
			"x-auth-token": "",
		},
	},
	headerFileServer: {
		headers: {
			"Content-Type": "multipart/form-data",
		},
	},
}

export default headerConfig
